import firebase from "firebase";

var firebaseConfig = {
    apiKey: "AIzaSyBufZGxE1UMoolAL4cRFd1zA0SdhWGZH_4",
    authDomain: "clone-de08e.firebaseapp.com",
    databaseURL: "https://clone-de08e.firebaseio.com",
    projectId: "clone-de08e",
    storageBucket: "clone-de08e.appspot.com",
    messagingSenderId: "742684546462",
    appId: "1:742684546462:web:73372be2f07833e2e1f28f"
  };


  const firebaseApp = firebase.initializeApp(firebaseConfig);

  const db = firebaseApp.firestore();
  const auth = firebase.auth();
  
  export { db, auth };
