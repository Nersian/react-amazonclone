import React from 'react'
import "./Home.css"
import Product from "./Product"

function Home() {
    return (
        <div className='home'>
            <div className='home__container'>
               <img className="home__image" 
               src="https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Fuji/2020/May/Hero/Fuji_TallHero_45M_v2_1x._CB432458380_.jpg" 
               alt=""/>
            

            <div className='home__row'>
                <Product id={1} title="The lean startup" price={29.99} 
                image="https://images-na.ssl-images-amazon.com/images/I/41kaOFDXzSL._AC_SY200_.jpg"
                rating = {5}/> 
                <Product id={2} title="Garnek Indukcja 16cm" price={14.99} 
                image="https://f00.esfr.pl/foto/7/34193328057/4e6a03eba101a83a67a55d1f77318b87/la-bonita-solo-16cm,34193328057_7.jpg"
                rating = {4}/> 
            </div>

            <div className='home__row'>
                <Product id={3} title="PlayStation 4" price={399.99} 
                image="https://image.ceneostatic.pl/data/products/47044577/i-sony-playstation-4-slim-1tb-czarny.jpg"
                rating = {4}/> 
                <Product id={4} title="Ultra Mikser" price={24.99} 
                image="https://ffstatic.pl/kitchenaid/kitchenaid-mikser-artisan-185-czerwony-karmel__66796_c5dd5de-s2500x2500.jpg" rating = {4}/> 
                <Product id={5} title="X Box Series S" price={299.99} 
                image="https://ocs-pl.oktawave.com/v1/AUTH_2887234e-384a-4873-8bc5-405211db13a2/spidersweb/2020/09/xbox-series-s-cena-w-polsce-1349-zl-1740x798.jpg"
                rating = {4}/> 
            </div>

            <div className='home__row'>
            
                <Product id={6} title="Geforce RTX 3090" price={1499} 
                image="https://www.purepc.pl/image/news/2020/06/10_nvidia_geforce_rtx_3080_kolejne_zdjecia_prezentujace_chlodzenie_0_b.jpg"
                rating = {5}/>  
            </div>
        </div>
        </div>
    )
}

export default Home

